You must have completed the http://go.gitforidex.com/ exercises in order to complete the final exercise.

Ready???


Clone the final exercise repo
```
git clone https://bitbucket.org/jeremygrajales/git-for-idex final-exercise
cd final-exercise
```

Remember to have you name and email setup. Your email must match the one you used for the exercises!
```
git config user.name "<YOUR NAME>"
git config user.email "<Same as exercises email>"
```

Create a feature branch for your changes.
Replace <YOUR USERNAME> with an appropriate value (e.g. jgrajales).
The actual value doesn't matter as long as it's unique.

```
git checkout -b feature/<YOUR USERNAME>
```

Open the root directory and browse to the "people" directory.
Copy the _template.json file into a new file in the same directory
with the following filename format: <YOUR USERNAME>.json

Replace the placeholder values with your name and email address.
This is what will appear on the leaderboard.


Add your changes
```
git add .
```

Commit your changes
```
git commit -m '<YOUR awesomely descriptive commit message>'
```

Push to the remote repository.
Replace <YOUR USERNAME> with an appropriate value (e.g. jgrajales).
```
git push origin feature/<YOUR USERNAME>
```

Use the following user credentials to push back up to Bitbucket when prompted:

Username: idextestuser
Password: gitforidex2018!

Checkout https://bitbucket.org/jeremygrajales/git-for-idex/addon/pipelines/home
to see your changes tested and deployed with Bitbucket Pipelines.

The actual update may take much long but you should see a "Status" message indicating
whether your changes were merged. This test will fail if you have not completed
prior exercises.