<?php

// get email from commit
$command = 'git show --no-patch --format="%ae" ${BITBUCKET_COMMIT}';
$email = exec($command);
$shaemail = sha1($email);

$userObject = file_get_contents('http://go.gitforidex.com/api/committer/' . $shaemail);
$userObject = preg_replace('/^.+\n/', '', $userObject);
$userObject = json_decode($userObject);

$completed = $userObject->passedExercises;

$prereqs = [
    'master',
    'commit-one-file',
    'commit-one-file-staged',
    'ignore-them',
    'chase-branch',
    'merge-conflict',
    'save-your-work',
    'change-branch-history',
    'remove-ignored',
    'case-sensitive-filename',
    'fix-typo'
];

$isLeader = true;
foreach($prereqs as $prereq) {
    if(!in_array($prereq, $completed)) {
        $isLeader = false;
        break;
    }
}
if($isLeader) {
exit();
}
else {
exit(1);
}