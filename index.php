<?php

require_once __DIR__ .'/vendor/autoload.php';

$loader = new Twig_Loader_Filesystem(__DIR__.'/templates');
$twig = new Twig_Environment($loader, array());

$users = [];
$directory = __DIR__ . '/people';
$scanned_directory = array_diff(scandir($directory), array('..', '.'));
foreach($scanned_directory as $json) {
    if($json[0] != "_") {
        $contents = file_get_contents(__DIR__ . '/people/' . $json);
        $obj = json_decode($contents);
        $users[] = $obj;
    }
}

if(url('/')) {
    $leaderboard = view('leaderboard', ['users' => $users], false);
    view('index', array('leaderboard' => $leaderboard));
}
else if(url('/leaderboard')) {
    view('leaderboard', ['users' => $users]);
}

function url($url) {
    return $url == parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
}

function view($template = 'index', $args = array(), $show = true) {
    global $twig;
    $template = $twig->load($template . '.html');
    $output = $template->render($args);
    if($show) {
        echo $output;
    }
    return $output;
}